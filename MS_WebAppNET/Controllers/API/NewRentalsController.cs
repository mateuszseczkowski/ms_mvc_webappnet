﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MS_WebAppNET.DTOs;
using MS_WebAppNET.Models;

namespace MS_WebAppNET.Controllers.API
{
    public class NewRentalsController : ApiController
    {

        private readonly ApplicationDbContext _context;

        public NewRentalsController()
        {
            _context = new ApplicationDbContext();
        }

        [HttpPost]
        public HttpResponseMessage CreateNewRentals(NewRentalDto newRental)
        {
/*            if (newRental.MovieIds.Count == 0)
                return Request.CreateResponse(HttpStatusCode.BadRequest, "MovieIds list is empty.");*/

/*            var customer = _context.Customers.SingleOrDefault(c => c.Id == newRental.CustomerId);*/
            var customer = _context.Customers.Single(c => c.Id == newRental.CustomerId);

/*            if (customer == null)
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Customer Id is not valid.");*/

            var movies = _context.Movies.Where(m => newRental.MovieIds.Contains(m.Id)).ToList();

/*            if (movies.Count() != newRental.MovieIds.Count)
                return Request.CreateResponse(HttpStatusCode.BadRequest, "One or more movieIds are invalid");*/

            foreach (var movie in movies)
            {
                if (movie.NumberAvailable == 0)
                    return Request.CreateResponse(HttpStatusCode.BadRequest, movie.Name + " - Movie is not available");

                movie.NumberAvailable--;

                var rental = new Rental()
                {
                  Customer = customer,
                  Movie = movie,
                  DateRented = DateTime.Now,
                };

                _context.Rentals.Add(rental);
            }
            _context.SaveChanges();

            return Request.CreateResponse(HttpStatusCode.OK);
        }

    }
}
