﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MS_WebAppNET.Startup))]
namespace MS_WebAppNET
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
