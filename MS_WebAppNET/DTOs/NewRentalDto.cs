﻿using System;
using System.Collections.Generic;

namespace MS_WebAppNET.DTOs
{
    public class NewRentalDto
    {
        public int CustomerId { get; set; }
        public List<int> MovieIds { get; set; }
    }
}