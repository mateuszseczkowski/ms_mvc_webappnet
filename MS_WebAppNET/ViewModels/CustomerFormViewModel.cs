﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MS_WebAppNET.Models;

namespace MS_WebAppNET.ViewModels
{
    public class CustomerFormViewModel
    {
        public IEnumerable<MembershipType> MembershipTypes { get; set; }
        public Customer Customer { get; set; }
    }
}