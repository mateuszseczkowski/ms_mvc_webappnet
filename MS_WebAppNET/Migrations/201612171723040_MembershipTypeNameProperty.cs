namespace MS_WebAppNET.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MembershipTypeNameProperty : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MembershipTypes", "Name", c => c.String());
            Sql("Update MembershipTypes set Name = 'New user' where Id like '1'");
            Sql("Update MembershipTypes set Name = 'Monthly membership' where Id like '2'");
            Sql("Update MembershipTypes set Name = 'Old user' where Id like '3'");
            Sql("Update MembershipTypes set Name = 'Very advanded meloman' where Id like '4'");
        }
        
        public override void Down()
        {
            DropColumn("dbo.MembershipTypes", "Name");
        }
    }
}
