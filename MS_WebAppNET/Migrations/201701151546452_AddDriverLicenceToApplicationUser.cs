namespace MS_WebAppNET.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDriverLicenceToApplicationUser : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "DrivingLicence", c => c.String(nullable: false, maxLength: 255));
        }
        
        public override void Down()
        {
            AddColumn("dbo.AspNetUsers", "DriverLicence", c => c.String(nullable: false, maxLength: 255));
        }
    }
}
