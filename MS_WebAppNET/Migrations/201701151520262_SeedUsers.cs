namespace MS_WebAppNET.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SeedUsers : DbMigration
    {
        public override void Up()
        {
            Sql(@"
INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'7f476bbc-25ca-4475-ac76-70e62b3bcf96', N'admin@ms.com', 0, N'AK9yKfKaFI02Cv5RHoQusa+9+nywvdPjpZ+Kg1l+l3x7RGFZXhtZUJIPi3Fv9auzNg==', N'dc00c67d-0805-48dd-91a3-5f74c3be07d1', NULL, 0, 0, NULL, 1, 0, N'admin@ms.com')
INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'c7790d9c-5ee7-48cd-aead-e07dc2921088', N'guest@ms.com', 0, N'ALDYMMLnfvgBReuIeD8cWY34WZYowEtCZioMgQ4R8vm7/ApF/UislCHjRDMzZQVJ2w==', N'0355e532-24b4-4015-9b56-a0e5dd9c732c', NULL, 0, 0, NULL, 1, 0, N'guest@ms.com')

INSERT INTO [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'8c8ea703-6cb1-4f44-b667-6c21dab3d365', N'CanManageMovies')

INSERT INTO [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'7f476bbc-25ca-4475-ac76-70e62b3bcf96', N'8c8ea703-6cb1-4f44-b667-6c21dab3d365')
");
        }
        
        public override void Down()
        {

        }
    }
}
