namespace MS_WebAppNET.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PopulateGenres : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT into Genres Values ('1', 'Horror')");
            Sql("INSERT into Genres Values ('2', 'Dramatic')");
            Sql("INSERT into Genres Values ('3', 'Romance')");
            Sql("INSERT into Genres Values ('4', 'Action')");
            Sql("INSERT into Genres Values ('5', 'Comedy')");
            Sql("INSERT into Genres Values ('6', 'Family')");
            Sql("INSERT into Genres Values ('7', 'Kids')");
        }
        
        public override void Down()
        {
        }
    }
}
