namespace MS_WebAppNET.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MoviesGiveValuesToEmptyFields : DbMigration
    {
        public override void Up()
        {
            Sql("Update Movies set Name = 'empty' where Name is NULL");
            Sql("Update Movies set ReleaseDate = '01.01.1900 00:00:00' where ReleaseDate is NULL");
            Sql("Update Movies set DateAdded = '01.01.1900 00:00:00' where DateAdded is NULL");
        }
        
        public override void Down()
        {
        }
    }
}
