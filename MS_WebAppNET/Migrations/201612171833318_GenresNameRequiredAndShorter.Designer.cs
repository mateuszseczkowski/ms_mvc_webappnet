// <auto-generated />
namespace MS_WebAppNET.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class GenresNameRequiredAndShorter : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(GenresNameRequiredAndShorter));
        
        string IMigrationMetadata.Id
        {
            get { return "201612171833318_GenresNameRequiredAndShorter"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
