﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace MS_WebAppNET
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            var settings = config.Formatters.JsonFormatter.SerializerSettings;
            
            settings.ContractResolver =  new CamelCasePropertyNamesContractResolver(); 
            settings.Formatting = Formatting.Indented;
            // lowercase of json objects names first letters (CAMEL notation instead of PASCAL notation)
            // this is needed in javascript

            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
